package com.test.app;

import java.util.Objects;

public class Mower {

    private int xCoordinate;
    private int yCoordinate;
    private Direction cardinalDirection;
    private final Lawn lawn;

    Mower(Lawn lawn, int xCoordinate, int yCoordinate, Direction cardinalDirection) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.cardinalDirection = cardinalDirection;
        this.lawn = lawn;
    }

    // R command
    void moveClockwise() {
        cardinalDirection = Direction.valueOf((cardinalDirection.getValue()+3)%4);
    }

    // L command
    void moveCounterClockwise() {
        cardinalDirection = Direction.valueOf((cardinalDirection.getValue()+1)%4);
    }

    void moveForward() {
        switch (cardinalDirection){
            case E: xCoordinate = xCoordinate+1 > lawn.getxCoordinateUpper() ? xCoordinate : xCoordinate + 1;
                        break;
            case W: xCoordinate = xCoordinate-1 < lawn.getxCoordinateLower() ? xCoordinate : xCoordinate -1;
                        break;
            case S: yCoordinate = yCoordinate-1 < lawn.getyCoordinateLower() ? yCoordinate : yCoordinate - 1;
                        break;
            default: yCoordinate = yCoordinate+1 > lawn.getyCoordinateUpper() ? yCoordinate : yCoordinate + 1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mower mower = (Mower) o;
        return xCoordinate == mower.xCoordinate &&
                yCoordinate == mower.yCoordinate &&
                cardinalDirection == mower.cardinalDirection;
    }

    @Override
    public int hashCode() {

        return Objects.hash(xCoordinate, yCoordinate, cardinalDirection);
    }

    @Override
    public String toString() {
        return xCoordinate + " " + yCoordinate + " " + cardinalDirection;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public Direction getCardinalDirection() {
        return cardinalDirection;
    }
}
