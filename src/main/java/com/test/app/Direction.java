package com.test.app;

import java.util.HashMap;
import java.util.Map;

public enum Direction {
    N(0),
    W(1),
    S(2),
    E(3);

    private int value;
    private static Map<Integer, Direction> map = new HashMap<Integer, Direction>();

    Direction(int value) {
        this.value = value;
    }

    static {
        for (Direction direction : Direction.values()) {
            map.put(direction.value, direction);
        }
    }

    public static Direction valueOf(int pageType) {
        return map.get(pageType);
    }

    public int getValue() {
        return value;
    }
}
