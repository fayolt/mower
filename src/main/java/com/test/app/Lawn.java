package com.test.app;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class Lawn {
    private static final int xCoordinateLower = 0;
    private static final int yCoordinateLower = 0;
    private int xCoordinateUpper;
    private int yCoordinateUpper;
    private List<Pair<Mower, String>> mowers;

    Lawn() {
    }

    Lawn(InputStream inputStream){
        try {
            mowers = new ArrayList<>();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader br = new BufferedReader(inputStreamReader);
            String line = br.readLine();
            int lineCounter = 0;
            String [] data;
            Mower mower = null;
            while (line != null) {
                if (lineCounter == 0) {
                    data = line.split(" ");
                    setxCoordinateUpper(Integer.parseInt(data[0]));
                    setyCoordinateUpper(Integer.parseInt(data[1]));
                }
                if ((lineCounter != 0) && (lineCounter % 2 == 1)) {
                    data = line.split(" ");
                    mower = new Mower(this, Integer.parseInt(data[0]), Integer.parseInt(data[1]), Direction.valueOf(data[2]));
                }
                if ((lineCounter != 0) && (lineCounter % 2 == 0)){
                    Pair<Mower, String> pair = new Pair<>(mower, line);
                    mowers.add(pair);
                }
                lineCounter++;
                line = br.readLine();
            }

        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }

    }

    void operateMowers() {
        for (Pair pair: mowers) {
            operateMower(pair);
        }
    }

    private void operateMower(Pair<Mower, String> pair) {
        for (int i=0; i<pair.getSecond().length(); i++) {
            char c = pair.getSecond().charAt(i);
            switch (c) {
                case 'R': pair.getFirst().moveClockwise(); break;
                case 'L': pair.getFirst().moveCounterClockwise(); break;
                default: pair.getFirst().moveForward();
            }
        }
        System.out.println(pair.getFirst().toString());
    }

    int getxCoordinateLower() {
        return xCoordinateLower;
    }

    int getyCoordinateLower() {
        return yCoordinateLower;
    }

    int getxCoordinateUpper() {
        return xCoordinateUpper;
    }

    void setxCoordinateUpper(int xCoordinateUpper) {
        this.xCoordinateUpper = xCoordinateUpper;
    }

    int getyCoordinateUpper() {
        return yCoordinateUpper;
    }

    void setyCoordinateUpper(int yCoordinateUpper) {
        this.yCoordinateUpper = yCoordinateUpper;
    }

    void setMowers(List<Pair<Mower, String>> mowers) {
        this.mowers = mowers;
    }

    public List<Pair<Mower, String>> getMowers() {
        return mowers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lawn lawn = (Lawn) o;
        return xCoordinateUpper == lawn.xCoordinateUpper &&
                yCoordinateUpper == lawn.yCoordinateUpper &&
                Objects.equals(mowers, lawn.mowers);
    }

    @Override
    public int hashCode() {

        return Objects.hash(xCoordinateUpper, yCoordinateUpper, mowers);
    }

    @Override
    public String toString() {
        return "Lawn{" +
                "xCoordinateLower=" + xCoordinateLower +
                ", yCoordinateLower=" + yCoordinateLower +
                "xCoordinateUpper=" + xCoordinateUpper +
                ", yCoordinateUpper=" + yCoordinateUpper +
                ", mowers=" + mowers +
                '}';
    }
}
