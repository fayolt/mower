package com.test.app;

import java.io.FileInputStream;
import java.io.InputStream;

public class App 
{

    public static void main( String[] args )
    {
        InputStream inputStream = null;
        try {
            inputStream = args.length > 0 ?
                    new FileInputStream(args[0]): App.class.getResourceAsStream("/init.txt");
            Lawn lawn = new Lawn(inputStream);
            lawn.operateMowers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
