package com.test.app;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

public class AppTest 
{
    @Test
    public void testExtractInitDataFromFile()
    {
        Lawn actualLawn = new Lawn();
        actualLawn.setyCoordinateUpper(5);
        actualLawn.setxCoordinateUpper(5);

        Mower mower = new Mower(actualLawn, 1, 2, Direction.N);
        String instructions = "LFLFLFLFF";
        List<Pair<Mower, String>> mowers = new ArrayList<>();
        Pair<Mower, String> mowerStringPair = new Pair<>(mower, instructions);
        mowers.add(mowerStringPair);
        actualLawn.setMowers(mowers);

        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("init_test_1.txt");
        Lawn expectedLawn = new Lawn(inputStream);

        assertThat(expectedLawn).isEqualTo(actualLawn);
    }

    @Test
    public void testMowerOperation()
    {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("init_test.txt");

        Lawn expectedLawn = null;
        try {
            expectedLawn = new Lawn(inputStream);
            expectedLawn.operateMowers();

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        Mower firstMower = new Mower(expectedLawn, 1, 3, Direction.N);
        Mower lastMower = new Mower(expectedLawn, 5, 1, Direction.E);

        assertThat(expectedLawn.getMowers().get(0).getFirst()).isEqualTo(firstMower);
        assertThat(expectedLawn.getMowers().get(expectedLawn.getMowers().size()-1).getFirst()).isEqualTo(lastMower);
    }

    @Test
    public void testMoveClockwise() {
        Lawn actualLawn = new Lawn();
        actualLawn.setyCoordinateUpper(5);
        actualLawn.setxCoordinateUpper(5);

        Mower mower = new Mower(actualLawn, 1, 2, Direction.N);
        mower.moveClockwise();

        assertEquals(mower.getxCoordinate(), 1);
        assertEquals(mower.getyCoordinate(), 2);
        assertEquals(mower.getCardinalDirection(), Direction.E);
    }

    @Test
    public void testMoveCounterClockwise() {
        Lawn actualLawn = new Lawn();
        actualLawn.setyCoordinateUpper(5);
        actualLawn.setxCoordinateUpper(5);

        Mower mower = new Mower(actualLawn, 1, 2, Direction.N);
        mower.moveCounterClockwise();

        assertEquals(mower.getxCoordinate(), 1);
        assertEquals(mower.getyCoordinate(), 2);
        assertEquals(mower.getCardinalDirection(), Direction.W);
    }

    @Test
    public void testMoveForward() {
        Lawn actualLawn = new Lawn();
        actualLawn.setyCoordinateUpper(5);
        actualLawn.setxCoordinateUpper(5);

        Mower mower = new Mower(actualLawn, 1, 2, Direction.N);
        mower.moveForward();

        assertEquals(mower.getxCoordinate(), 1);
        assertEquals(mower.getyCoordinate(), 2+1);
        assertEquals(mower.getCardinalDirection(), Direction.N);
    }

    @Test
    public void testMoveNorthFromUpperRightCorner() {
        Lawn actualLawn = new Lawn();
        actualLawn.setyCoordinateUpper(5);
        actualLawn.setxCoordinateUpper(5);

        Mower mower = new Mower(actualLawn, 5, 5, Direction.N);
        mower.moveForward();

        assertEquals(mower.getxCoordinate(), 5);
        assertEquals(mower.getyCoordinate(), 5);
        assertEquals(mower.getCardinalDirection(), Direction.N);
    }

    @Test
    public void testMoveEastFromUpperRightCorner() {
        Lawn actualLawn = new Lawn();
        actualLawn.setyCoordinateUpper(5);
        actualLawn.setxCoordinateUpper(5);

        Mower mower = new Mower(actualLawn, 5, 5, Direction.E);
        mower.moveForward();

        assertEquals(mower.getxCoordinate(), 5);
        assertEquals(mower.getyCoordinate(), 5);
        assertEquals(mower.getCardinalDirection(), Direction.E);
    }

    @Test
    public void testMoveSouthFromLowerLeftCorner() {
        Lawn actualLawn = new Lawn();
        actualLawn.setyCoordinateUpper(5);
        actualLawn.setxCoordinateUpper(5);

        Mower mower = new Mower(actualLawn, 0, 0, Direction.S);
        mower.moveForward();

        assertEquals(mower.getxCoordinate(), 0);
        assertEquals(mower.getyCoordinate(), 0);
        assertEquals(mower.getCardinalDirection(), Direction.S);
    }

    @Test
    public void testMoveWestFromLowerLeftCorner() {
        Lawn actualLawn = new Lawn();
        actualLawn.setyCoordinateUpper(5);
        actualLawn.setxCoordinateUpper(5);

        Mower mower = new Mower(actualLawn, 0, 0, Direction.W);
        mower.moveForward();

        assertEquals(mower.getxCoordinate(), 0);
        assertEquals(mower.getyCoordinate(), 0);
        assertEquals(mower.getCardinalDirection(), Direction.W);
    }
}
