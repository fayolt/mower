# TheMower
An automatic lawn mower designed to mow rectangular surfaces.

To Build the app:

  * On Linux use `./mvnw clean package`
  * On Windows use `./mvnw.cmd clean package`

To start the app:

  * move to the target directory `cd target/`
  * execute with default input file `java -jar jarfile`
  * with a provided input file `java -jar jarfile path/to/input/file`

Default input file:  

  * Content:
    
          5 5
          1 2 N
          LFLFLFLFF
          3 3 E
          FFRFFRFRRF
  
  * Expected output:
  
        1 3 N
        5 1 E

